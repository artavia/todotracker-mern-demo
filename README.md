# Description
Prototype todo tracker-- MERN stack exercise --for use with Heroku and Mongo Atlas.

## Addendum Number One with Justification
This project has been updated for use subsequent to December 2022. I had five (5) projects simultaneously go to the pot given that Heroku brought Stack 18 to [EOL as explained here](https://help.heroku.com/X5OE6BCA/heroku-18-end-of-life-faq "link to Heroku Help"), hence, a slight re-working of said projects (this is no exception).

## Updated status (2023) ~~Please visit MERN Todo App~~
Sorry but subsequent to December 2022 and as a result of the new policy at SalesForce/Heroku, free plans are no longer available to the general public. Thus, the site will be left in an inoperable state. ~~You can experiment with and/or view [the demo which is found at heroku](https://todotracker-mern-demo.herokuapp.com "Link to MERN Todo App") and decide if this is something for you to play around with at a later date.~~

## The accompanying project (2023)
You can, however, visit the accompanying &quot;sister&quot; project only that *Netlify* was utilized instead of *Heroku*. It&rsquo;s the same project but the stack is different on the back&#45;end. That project is also hosted at [Gitlab](https://gitlab.com/artavia/todotracker-faas-demo "link to repo") and a public-facing demo version is at [Netlify](https://todotracker-faas-demo.netlify.app/ "link to demo"). That&rsquo;s all. Thanks.

## Final Addendum (2023)
This project utilized Heroku. And it will be currently mothballed as a result of *limited resources*. The five affected projects in question are:
  - [marvel-demo-two-crypto-module-version](https://gitlab.com/artavia/marvel-demo-two-crypto-module-version "link to Gitlab Repository");
  - [marvel-demo-three-webpack-version](https://gitlab.com/artavia/marvel-demo-three-webpack-version "link to Gitlab Repository");
  - [issuetracker-mean-demo](https://gitlab.com/artavia/issuetracker-mean-demo "link to Gitlab Repository");
  - [todotracker-mern-demo](https://gitlab.com/artavia/todotracker-mern-demo "link to Gitlab Repository");
  - [nacho-ordinary-mehn-example](https://gitlab.com/artavia/nacho-ordinary-mehn-example "link to Gitlab Repository");