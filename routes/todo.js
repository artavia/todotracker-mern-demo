// BASE SETUP
// =============================================
const express = require("express");
const todoRouter = express.Router();

// MODEL DEFINITION
// =============================================
const Todo = require("../custom_models_mongoose/todo.model");

// ROUTING ASSIGNMENTS
// =============================================
todoRouter.get( "/" , ( req, res ) => {
  Todo.find( (err, todos) => {
    if(err){ // console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data." } );
    }
    res.status(200).json( todos );
  } );
} );

todoRouter.get( "/:id", ( req, res ) => {
  
  let id = req.params.id;

  Todo.findById( id, ( err, todo ) => {
    if(err){ // console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data." } );
    }
    res.status(200).json(todo);
  } );
} );

todoRouter.post( "/add" , ( req, res ) => {

  /*
  {
    "todo_description" : "Eat more ice cream"
    , "todo_responsible" : "Lucho"
    , "todo_priority" : "Low"
    , "todo_completed" : false
  }
  */

  // console.log( "req.body" , req.body ); 
  
  let todo = new Todo( req.body );
  todo.save( ( err, registeredTodo ) => {
    if(err){ // console.log( "err", err );
      res.status(500).json( { errormessage:  "Internal Error -- problem in posting data." } );
    }
    res.status(200).json( { message : "Todo has been added" } );
  } );

} );


todoRouter.post( "/update/:id" , ( req, res ) => {
  
  let id = req.params.id;
  Todo.findById( id, ( err, todo ) => {
    if(!todo){
      return res.status(404).send( { errormessage : "Todo not found" } );
    }

    todo.todo_description = req.body.todo_description;
    todo.todo_responsible = req.body.todo_responsible;
    todo.todo_priority = req.body.todo_priority;
    todo.todo_completed = req.body.todo_completed;

    todo.save( ( err, updatedTodo ) => {
      if(err){ // console.log( "err", err );
        res.status(500).json( { errormessage:  "Internal Error -- problem in updating data." } );
      }
      res.status(200).json( { message : "Todo has been updated" } );
    } );

  } );
} );

todoRouter.get( "/delete/:id", ( req, res ) => {

  let idObjParameter = { _id: req.params.id };
  
  Todo.findByIdAndRemove( idObjParameter, ( err, todo ) => {
    if(err){ // console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data." } );
    }
    res.status(200).json( { message : "Todo removed successfully" } );
  } );

} );

module.exports = todoRouter;