import React from 'react';

import { Nav } from "./nav";
import { Footer } from "./footer";

const Main = ( props ) => {
  
  // console.log( 'Main' , props );
  // console.log( 'Main' , props.children );
  
  let element = (
    <div className="container">
      <Nav />
      <main role="main">
        { props.children }
      </main>
      <Footer />
    </div>
  );

  return element;
};

export {Main};