import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import { Redirect } from 'react-router-dom';

import { TodosList } from "../functional-components/todos-list";

import { AdminCreateTodo } from "../functional-components/create-todo";
import { AdminEditTodo } from "../functional-components/edit-todo";

// import { EventsPage } from '../functional-components/events-page';
import { RegisterPage } from '../functional-components/register-page';
import { LoginPage } from '../functional-components/login-page';
import { AdminSpecialEventsPage } from '../functional-components/special-events-page';

import { DNEPage } from '../DNE/DNEPage';
import { Main } from './main';

const Alpha = () => {
  
  let element = ( 
    <BrowserRouter>
      <Main>
        <Switch>
          
          <Route path="/" exact component={TodosList} />
          <Route path="/create" component={AdminCreateTodo} />
          <Route path="/edit/:id" component={AdminEditTodo} />

          {/* <Redirect from="/" to="/events" /> */}

          {/* <Route path="/events" exact component={EventsPage} /> */}
          <Route path="/login" component={LoginPage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/special" component={AdminSpecialEventsPage} />

          {/* <Redirect from="/" to="/events" /> */}

          <Route component={ DNEPage } />

        </Switch>
      </Main>
    </BrowserRouter>
  );
  
  return element;

};

export {Alpha};