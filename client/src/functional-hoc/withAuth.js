import React , { useState } from 'react';
import { useEffect , useCallback } from 'react';

import { useHistory } from 'react-router-dom';
import { AuthService } from "../services/AuthService";

const withAuth = ( AuthComponent ) => {
  
  const Auth = new AuthService();
  
  return function AuthWrapped(props){

    // console.log( "props: " , props );
    
    // state = { profile: null };
    const [ profile, setProfile ] = useState( null );
    
    let history = useHistory(); // console.log( "history" , history );
    
    let innerFunction = useCallback( () => {
      if( !Auth.loggedIn() ){
        history.replace('/login');
      }
      else
      if( Auth.loggedIn() ){
        try{
          const userprofile = Auth.getProfile(); // console.log( "profile" , profile );
          setProfile( userprofile );
        }
        catch(error){ // console.log( "error", error );
          Auth.logout();
          history.replace('/login');
        }
      }
    } , [ history ] );

    useEffect( () => {

      innerFunction();
      return () => {};

    } , [ innerFunction ] );
    

    let element = (
      <AuthComponent history={ history } profile={ profile } {...props} />
    );

    if( profile ){
      return element;
    }
    else{
      return null;
    }

  };

};

export { withAuth };