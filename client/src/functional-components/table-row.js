import React , { useState } from 'react';
import { NavLink, Redirect } from 'react-router-dom';
import { TodoService } from '../services/TodoService';
import { AuthService } from '../services/AuthService';

const TableRow = ( props ) => {

  // console.log( "props", props ); 
  // console.log( "props.todo._id", props.todo._id ); 

  const TServ = new TodoService();
  const Auth = new AuthService();

  const [ redirect, setRedirect ] = useState( false );
  const [ error, setError ] = useState(false);

  const deleteRecord = async (event) => {

    event.preventDefault();

    let id = props.todo._id; 

    let deleteonetodopromise = await TServ.deleteOneTodo( id )
    .then( ( data ) => { // console.log( "data" , data );
      return data;
    } )
    .catch( ( err ) => { // console.log( "err", err );
      setError( err.message );
    } );

    // console.log( "deleteonetodopromise" , deleteonetodopromise );

    if( deleteonetodopromise !== undefined){
      setRedirect( true );
    }

  };

  if( redirect ){
    return ( <Redirect to='/' /> );
  }

  let errormessage = (
    <tr>
      <td colSpan="5"> <strong>An error has occured: </strong> { error } </td>
    </tr>
  );

  // console.log( "Auth.loggedIn()" , Auth.loggedIn() );

  let element = (
    <>
      <tr>
        <td className={ props.todo.todo_completed ? 'completed' : '' } >{props.todo.todo_description}</td>
        <td className={ props.todo.todo_completed ? 'completed' : '' }>{props.todo.todo_responsible}</td>
        <td className={ props.todo.todo_completed ? 'completed' : '' }>{props.todo.todo_priority}</td>
        
        { !Auth.loggedIn() && <><td>
          &nbsp;
        </td>
        <td>
          &nbsp;
        </td></> }

        { Auth.loggedIn() && <><td>
          <NavLink to={`/edit/${props.todo._id}`}>
            Edit
          </NavLink>
        </td>
        <td>
          <NavLink className="danger" onClick={ deleteRecord } to="#" >
            Delete
          </NavLink>
        </td></> }

      </tr>
      

      { !!error && errormessage }

      

    </>
  );
  return element; 

};

export { TableRow };