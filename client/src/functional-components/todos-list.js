import React from 'react';
import { useState, useEffect } from 'react';
import { useCallback } from 'react';
import { NavLink } from 'react-router-dom';
import { TableRow } from './table-row';
import { TodoService } from '../services/TodoService';

// import { CustomError } from '../functional-components/custom-error';

const TodosList = () => {

  // console.log( "this", this ); // undefined
  // console.log( "React", React ); // yada, yada, yada

  const TServ = new TodoService();
  const [ todos, setTodos ] = useState( [] );
  const [ error, setError ] = useState(false);

  let innerFunction = useCallback( async () => {
    const abortController = await new AbortController();
    const signal = abortController.signal;

    const alltodospromise = await TServ.getAllTodos( signal ).catch( (err) => {
      //console.log( "err", err );
      setError( err.toString() );
    } ); 

    // console.log( "alltodospromise" , alltodospromise );

    if( alltodospromise !== undefined ){
      try{ 
        // return setTodos( await alltodospromise );
        // setTodos( alltodospromise );
        setTodos( alltodospromise );
        return () => {
          // abortController.abort();
          signal.abortController();
        };
      }
      catch( err ){
        // console.log( "err", err );
        setError( err.message );
      }
    }
    
  } , [] ); 
  // first NO LEAKS BUT WONKY
  // TESTED... -WITH [] AND just setTodos()... as in either without the return prefacing FN name or just  return setTodos() sans the return STMT... AND react-hooks/exhaustive-deps AND - "To ignore, add // eslint-disable-next-line to the line before." -  
  // Compiled with warnings. <React Hook useCallback has a missing dependency: 'TServ'. Either include it or remove the dependency array/>
  
  // } , [ TServ ] ); // second with ... 'TServ' dependency LEAKS

  useEffect( () => { 
    innerFunction();
    return () => {}; 
  } , [ innerFunction ] );

  const todoList = () => {
    return todos.map( mapTodos );
  };

  const mapTodos = (el,idx,arr) => {
    return <TableRow key={idx} todo={el} />
  };

  const noOpLink = (event) => {
    event.preventDefault(); 
  };

  let errormessage = (
    <>
      <div className="starter-template danger">
        <h2>An error has occurred</h2>
        <p>{ error }</p>
      </div>
    </>
  );

  let element = (
    <>
      <div className="jumbotron">
        <div className="container">
          <h1 className="display-3">Todos List</h1>
          <p><NavLink className="btn btn-primary btn-lg" onClick={ noOpLink } to="#" role="button">Learn more &raquo;</NavLink></p>
        </div>
      </div>

      <div className="container">

        <table className="table table-striped">
          <thead>
            <tr>
              <th>Description</th>
              <th>Responsible</th>
              <th>Priority</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {/* { todoList() } */}
            { todoList() }
          </tbody>
        </table>

      </div>

      { !!error && errormessage }
      
    </>
  );
  return element;
};


export { TodosList };