db.todos.drop();
db.users.drop();
db.events.drop();
db.specialevents.drop();

db.events.insert( {
	name : "Alpha Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Beta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Charlie Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Delta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Elephant Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Foxtrot Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Alpha Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Beta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Charlie Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Delta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Elephant Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Foxtrot Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );




db.todos.insert( {
	todo_completed : true
  , todo_description : "Staple some more papers together because chances are I was going to do so anyways."
  , todo_responsible : "Lucho"
  , todo_priority : "Low"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "Build an electric car constituted of red and blue lego pieces."
  , todo_responsible : "Lucho"
  , todo_priority : "Medium"
} );

db.todos.insert( {
	todo_completed : true
  , todo_description : "Go swim in the ocean and swim back to shore."
  , todo_responsible : "Lucho"
  , todo_priority : "High"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "Go crawfish hunting"
  , todo_responsible : "Lucho"
  , todo_priority : "High"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "Play the cello"
  , todo_responsible : "Lucho"
  , todo_priority : "Medium"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "You have now chickity chinese chicken?"
  , todo_responsible : "Lucho"
  , todo_priority : "Medium"
} );

db.todos.insert( {
	todo_completed : true
  , todo_description : "Go play in mud"
  , todo_responsible : "Lucho"
  , todo_priority : "Low"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "Make a jar of pickles"
  , todo_responsible : "Lucho"
  , todo_priority : "High"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "Prepare specialty dish called spinach with sardine sandwiches"
  , todo_responsible : "Lucho"
  , todo_priority : "Low"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "Prepare jar of sun-dried tomatoes"
  , todo_responsible : "Lucho"
  , todo_priority : "Low"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "Staple some more paper"
  , todo_responsible : "Lucho"
  , todo_priority : "High"
} );

db.todos.insert( {
	todo_completed : true
  , todo_description : "Change out of my pajamas and clean up"
  , todo_responsible : "Lucho"
  , todo_priority : "High"
} );

db.todos.insert( {
	todo_completed : false
  , todo_description : "Praise Christ"
  , todo_responsible : "Lucho"
  , todo_priority : "High"
} ); // fin. of all inserts for any collection